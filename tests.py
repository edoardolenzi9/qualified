#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = 'Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'WTFPL-2.0'
__copyright__ = 'Copyleft 2021, lenzi.edoardo'


# load .env configs
from qualified.utils.env import Env
Env.load()

import unittest

from tests import Exercise1Test
from tests import Exercise2Test
from tests import Exercise3Test
from tests import Exercise4Test

'''Run all tests in tests/'''

unittest.main()