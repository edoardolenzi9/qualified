# Qualified Challenge


## Get Started

(Optional) Setup a virtual environment

```sh
python3 -m pip install --user virtualenv
python3 -m venv env
source env/bin/activate
```

Install dependencies 

```sh
python3 -m pip install -r requirements.txt
```

## Check Installation


1. Check script (print the available CLI-commands)

```sh
python3 qualified.py -h
```

2. Run tests

```sh
python3 tests.py 
```

Thanks,
Qualified Team :)
