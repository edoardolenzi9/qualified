#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = 'Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'WTFPL-2.0'
__copyright__ = 'Copyleft 2021, lenzi.edoardo'


import subprocess
import argparse
import sys
import os 

from qualified.cli_handler import CliHandler


class Parser(object):
    '''Cli entry point of the script, based on the library argparse
    see also: https://docs.python.org/3.9/library/argparse.html'''

    def __init__(self):
        self.parser = argparse.ArgumentParser(
            formatter_class=argparse.RawDescriptionHelpFormatter, 
            description = '''             Welcome to Qualified Script :)  

+------------------------------------------------------------+
|  ________               .__  .__  _____.__           .___  |
|  \_____  \  __ _______  |  | |__|/ ____\__| ____   __| _/  |
|   /  / \  \|  |  \__  \ |  | |  \   __\|  |/ __ \ / __ |   |
|  /   \_/.  \  |  // __ \|  |_|  ||  |  |  \  ___// /_/ |   |
|  \_____\ \_/____/(____  /____/__||__|  |__|\___  >____ |   | 
|         \__>          \/                       \/     \/   |
+------------------------------------------------------------+''',
            epilog = 'Source: https://gitlab.com/edoardolenzi9/qualified' )

        for i in range(1,5):
            self.parser.add_argument(f'-ex{i}', f'--exercise{i}', dest=f'exercise{i}', action='store_const',
                                    const=True, default=False,
                                    help=f'Run exercise{i}')


    def parse(self):    
        return self.parser.parse_args()


    def parse_args(self, command):    
        return self.parser.parse_args(command)