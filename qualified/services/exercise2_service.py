#!/usr/bin/env python3
# coding: utf-8

'''Exercise2 Solver'''

__author__ = 'Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'WTFPL-2.0'
__copyright__ = 'Copyleft 2021, lenzi.edoardo'

import json
import logging
LOG = logging.getLogger(__name__)


class Exercise2Service():

    '''Exercise 2 Service'''

    def __init__(self):
        LOG.info('Init Exercise2Service')