#!/usr/bin/env python3
# coding: utf-8

'''Exercise4 Solver'''

__author__ = 'Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'WTFPL-2.0'
__copyright__ = 'Copyleft 2021, lenzi.edoardo'

import json
import logging
LOG = logging.getLogger(__name__)


class Exercise4Service():

    '''Exercise 4 Service'''

    def __init__(self):
        LOG.info('Init Exercise4Service')