#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = 'Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'WTFPL-2.0'
__copyright__ = 'Copyleft 2021, lenzi.edoardo'

# load .env configs
from qualified.utils.env import Env
Env.load()
from qualified.utils.logger import Logger
LOG = Logger.getLogger(__name__)

from qualified.services.exercise1_service import Exercise1Service
from qualified.services.exercise2_service import Exercise2Service
from qualified.services.exercise3_service import Exercise3Service
from qualified.services.exercise4_service import Exercise4Service
import qualified.utils.localizations as loc
import qualified.utils.file_manager as fm
from qualified.domain.enums import *

from os import path
import sys
import os 


class CliHandler(object):

    '''Cli business logic, given the arguments typed
    calls the right handlers/procedures of the pipeline'''


    def __init__(self, args):
        if args.exercise1:
            self.exercise1_handler()
        elif args.exercise2:
            self.exercise2_handler()
        elif args.exercise3:
            self.exercise3_handler()
        elif args.exercise4:
            self.exercise4_handler()
        else:
            self.default_handler()


    # Command Handlers 

    def default_handler(self):
        LOG.info('Welcome to Qualified script! Type -h for help \n\n' +
                 '[You have to type at least one command of the pipeline]\n')

    
    def exercise1_handler(self):
        LOG.info('Selected: Exercise 1')
        Exercise1Service()

    def exercise2_handler(self):
        LOG.info('Selected: Exercise 2')
        Exercise2Service()

    def exercise3_handler(self):
        LOG.info('Selected: Exercise 3')
        Exercise3Service()

    def exercise4_handler(self):
        LOG.info('Selected: Exercise 4')
        Exercise4Service()

# Used when spawned in a new process

if __name__ == '__main__':
    LOG.info(f'Subprocess started {sys.argv}')
    sys.stdout.flush()
    from qualified.cli import Parser
    args = Parser().parse()    
    CliHandler(args)