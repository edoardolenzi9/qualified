#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = 'Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'WTFPL-2.0'
__copyright__ = 'Copyleft 2021, lenzi.edoardo'


from qualified.domain.enums import *


class Entity(object):
    

    def __init__(self, id):
        self.id = id


    def __eq__(self, other):
        return self.id == other.id # and \