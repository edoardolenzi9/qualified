#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = 'Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'WTFPL-2.0'
__copyright__ = 'Copyleft 2021, lenzi.edoardo'


# load .env configs
from qualified.utils.env import Env
Env.load()


# load tests
import unittest

from integration_tests import RunExercisesTest


'''Run all tests in integration_tests/'''

unittest.main()