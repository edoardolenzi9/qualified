#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = 'Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'WTFPL-2.0'
__copyright__ = 'Copyleft 2021, lenzi.edoardo'


import unittest

from qualified.cli_handler import CliHandler
from qualified.cli import Parser


class RunExercisesTest(unittest.TestCase):
    '''Test all exercises'''


    def test_run_exercise1(self):
        self.__run_exercise(1)


    def test_run_exercise2(self):
        self.__run_exercise(2)


    def test_run_exercise3(self):
        self.__run_exercise(3)


    def test_run_exercise4(self):
        self.__run_exercise(4)


    def __run_exercise(self, exercise_id:str):
        command = f'-ex{exercise_id}'.split()
        args = Parser().parse_args(command)    
        CliHandler(args)
